
### 11.3.21
* Fix for dnd5e concentration expiry race condition causing effect not found errors.

### 11.3.20
* Fix for combatants without actors
* Return duration to seconds on combat end

### 11.3.19
* Include updated pt-BR.json

### 11.3.18
* Fix for combat end special durations bug.

### 11.3.17
* Fix for turnStartSource and combat end special duration expiry 

### 11.3.16
* Set expiry reasons for times-up expired effects

### 11.3.15
* Fix for more edge cases when doing marcro repeats/special duration start/end turn expiry.

### 11.3.14
* Added support for macro repeats start and end of turn.
* Fix for some edge case special duration expiry

### 11.3.13
* Fix for special duration turnEnd/turnEndSource expring too early.

### 11.3.12
* Added special duration expiry of enchantments.
* Fix for dependent enchantment effects not being deleted.

### 11.3.11
* Added ability to call start/end turn macros for effects that are disabled/suppressed.
  - requires dae 11.3.21 or later

### 11.3.10
* Some fixed for v12. Module marked as compatible with v12

### 11.3.9.1
* Fix for times up and dnd5e versions prior to 3.1

### 11.3.9
* Intercept dnd5 getDependents and remove any expired effects from the list, since they will be removed by times-up

### 11.3.8
* Change the effectQueue to a set of effect uuids.

### 11.3.7
* Fix an error where times-up would track non-transfer effects on item for expiry. Does not cause an error but the expiry queue is larger than it needs to be.

### 11.3.6
* Fix for recording disabled effects as potential expiry targets. No impact on module behaviour.

### 11.3.5
* Fix for source actor turn end special duration.

### 11.3.4
* Reimplement special durations for source actor turn start/end.

### 11.3.3
* Fix for start of turn/end of turn macro repeat not firing correctly.

### 11.3.2
* Fix for error thrown on startup if compendium effects were added to the expriy queue

### 11.3.1
- a couple of bug fixes

### 11.3.0
* Chages to support legacy transferral false. This is a complete rewrite of times-up.
* Non-transfer effects are removed on expiry.
* Transfer effects are disabled on expiry.
* When swithcing into/out of combat the duration of active non-transfer effects will converted to rounds/seconds as appropriate.
* If udpate passive effects is enabled the duration of passive effects will also be updated. (warning this changes the effect on the item in systems with legacy transferral false - like dnd5e 3.0).
  - Due to the processing in foundry duration.seconds will take precedence over rounds/turns which will be rewritten by times-up as needed if this options is enabled.
* If a combatant is added to / removed from combat the duration of effects is set to rounds / seconds as appropriate.
* Effects added during combat will also have their duration adjusted.
* This modifying seconds -> rounds/turns is so that short duration effects will expire on exactly the right turn. Effects with expiry in seconds will always expire at the start of the turn.
* The conversion to rounds from seconds will only happen if the duration in seconds converted to rounds is <= to the specified Max rounds setting (default 10).
